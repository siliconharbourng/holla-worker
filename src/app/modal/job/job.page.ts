import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/service/booking.service';
import { TokenService } from 'src/app/service/token.service';
import io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { ModalController } from '@ionic/angular';

const SOCKET = environment.socket;
@Component({
  selector: 'app-job',
  templateUrl: './job.page.html',
  styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {

  socket;
  public id;
  accepted = {
    status: 'accepted'
  };

  cancel = {
    status: 'rejected',
    cancelReason: {
      reason: 'I am no longer Interested'
    }
  };
  constructor(
    public book: BookingService,
    private token: TokenService,
    public modalController: ModalController) {
    this.socket = io(`${SOCKET}`);
   }

  ngOnInit() {
    this.token.Getpayload().then(data => {
      this.id = data;
      this.book.availableWork(data).subscribe(job => {
        var status = job[0].status;
        if (status != 'pending') {
          this.dimiss();
        }
       });
    });
    this.socket.on('refreshpage', data => {
      this.book.availableWork(this.id).subscribe(job => {
        var status = job[0].status;
        if (status != 'pending') {
          this.dimiss();
        }
      });
    });
  }

  accept() {
    this.book.status(this.id, this.accepted).subscribe(data => {
      this.socket.emit('refresh', {});
      this.dimiss();
    });
  }

  reject() {
    console.log(this.cancel);
    this.book.cancel(this.id, this.cancel).subscribe(data => {
      this.socket.emit('refresh', {});
      this.dimiss();
    });
  }

  dimiss() {
    this.modalController.dismiss();
  }
}
