import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const BASE_URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  saveLocation(payload, id) {
    this.httpClient.post(`${BASE_URL}/user/location/${id}`, payload).subscribe(data => {
      console.log(data);
    });
  }
}
