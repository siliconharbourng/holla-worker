import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { TokenService } from './token.service';
import { Notifications } from '../classes/notify';

const BASEL_URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticationState = new BehaviorSubject(false);

  constructor(
    private http: HttpClient,
    private platform: Platform,
    private router: Router,
    private token: TokenService,
    private notify: Notifications) {
      this.platform.ready().then(() => {
        this.checkToken();
        this.isAutheticated();
      });
     }

  checkToken() {
    this.token.getToken().then(token => {
      if (token) {
        return this.authenticationState.next(true);
      } else {
        return this.authenticationState.next(false);
      }
    });
  }


  register(payload) {
    const hint = 'You have Registered Successfully';
    this.http.post(`${BASEL_URL}/auth/register/worker`, payload).subscribe(data => {
      this.token.setToken(data[userToken()]);
      this.notify.presentToast(hint);
      this.authenticationState.next(true);
      this.router.navigateByUrl('/');
    }, err => {
      if (err.error.details) {
        const msg = 'Make sure you Input the correct details and retry again';
        this.notify.presentAlert(msg, 'Authentication Error');
      } else if (err.error.message) {
        this.notify.presentAlert(err.error.message, 'Authentication Error');
      }
    });
  }

  login(payload) {
    const hint = 'Welcome back, Nice to have you here';
    this.http.post(`${BASEL_URL}/auth/signin`, payload).subscribe(data => {
      this.token.setToken(data[userToken()]);
      this.notify.presentToast(hint);
      this.authenticationState.next(true);
      this.router.navigateByUrl('/');
    }, err => {
        if (err.error.details) {
          const msg = 'Make sure you Input the correct details and retry again';
          this.notify.presentAlert(msg, 'Authentication Error');
        } else if (err.error.message) {
          this.notify.presentAlert(err.error.message, 'Authentication Error');
        }
    });
  }

  isAutheticated() {
    return !!this.authenticationState.value;
  }
}

function userToken() {
  return 'token';
}
