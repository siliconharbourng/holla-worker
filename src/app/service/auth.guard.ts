import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;

    constructor(private auth: AuthService) {}

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
      return this.auth.isAutheticated();
    }
}
