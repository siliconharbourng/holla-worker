import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private storage: Storage) { }

  getToken() {
    return this.storage.get('token');
  }

  setToken(token) {
    return this.storage.set('token', token);
  }

  deleteToken() {
    return this.storage.remove('token');
  }

  async Getpayload() {
    const token = await this.storage.get('token');
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = JSON.parse(window.atob(payload));
    }

    return payload.id;
  }

}
