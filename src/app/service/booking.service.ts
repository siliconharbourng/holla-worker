import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const BASE_URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  availableWork(id) {
    return this.http.get(`${BASE_URL}/booking/request/${id}`);
  }

  status(id, payload) {
    return this.http.patch(`${BASE_URL}/booking/request/${id}`, payload);
  }

  cancel(id, payload) {
    return this.http.put(`${BASE_URL}/booking/request/${id}/cancel`, payload);
  }

}


