import { Component, OnInit } from '@angular/core';
import { BookingService } from '../service/booking.service';
import { TokenService } from '../service/token.service';
import io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { ModalController } from '@ionic/angular';
import { JobPage } from '../modal/job/job.page';

const SOCKET = environment.socket;
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  socket: any;
  id;
  status;
  request: boolean;
  client = {
    email: ''
  };
  constructor(
    private booking: BookingService,
    private token: TokenService,
    public modalController: ModalController) {
    this.socket = io(`${SOCKET}`);
    this.request = false;
  }

  ngOnInit() {
    this.token.Getpayload().then(data => {
      this.id = data;
      this.Availablejobs(data);
    });
    this.socket.on('refreshpage', data => {
      this.Availablejobs(this.id);
    });
    console.log(this.request);
    
  }

  book(id) {
    this.booking.availableWork(id).subscribe(data => {
      console.log(data);
    });
  }

  Availablejobs(id) {
    this.booking.availableWork(id).subscribe(book => {
      try {
        var status = book[0].status;
        this.status = status;
        console.log(status);
        if (status = 'pending') {
          this.presentModal();
          this.request = false;
        }
        if (status = 'accepted') {
          this.client.email = book[0].seekersId.email;
          this.request = true;
        }
        if (status == 'arrived') {
          this.client.email = book[0].seekersId.email;
          this.request = true;
        }
        if (status == 'inprogress') {
          console.log('inprogress');
        }
      } catch (error) {
        this.request = false;
      }
    });
  }

  arriveLocation() {
    this.booking.status(this.id, {status: 'arrived'}).subscribe(data => {
      console.log(data);
      this.socket.emit('refresh', {});
    });
  }

  rejectjob() {
    this.booking.cancel(this.id, { status: 'rejected', cancelReason: {reason: 'I am no longer Interested'}}).subscribe(data => {
      console.log(data);
      this.socket.emit('refresh', {});
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
    component: JobPage,
    componentProps: { value: 123 },
    cssClass: 'alert'
    });

    await modal.present();

  }
}
