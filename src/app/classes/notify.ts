import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule, AlertController, ToastController } from '@ionic/angular';


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
    ],
    declarations: []
})
export class Notifications {

    constructor(public alertController: AlertController, public toastController: ToastController) {}

    async presentAlert(message, header) {
        const alert = await this.alertController.create({
            header: `${header}`,
            message: `${message}`,
            buttons: ['OK']
        });
        await alert.present();
    }

    async presentToast(message) {
        const toast = await this.toastController.create({
            message: `${message}`,
            duration: 2000
        });
        await toast.present();
    }
}
